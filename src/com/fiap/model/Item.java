package com.fiap.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="item")
public class Item {
	private Long item_id;
	private String nome;
	private Long quantidade;
	private BigDecimal valUnitario;
	private BigDecimal valDesconto;
	private Pedido pedido;
	
	public Item(){}
	
	@Id
	@GeneratedValue
	@Column
	public Long getItemId() {
		return item_id;
	}
	public void setItemId(Long id) {
		this.item_id = id;
	}

	@Column
	public long getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(long quantidade) {
		this.quantidade = quantidade;
	}

	@ManyToOne
	@JoinColumn(name="pedidoId", insertable=true, updatable=true)
	public Pedido getPedido(){
		return pedido;
	}
	
	public void setPedido(Pedido pedido){
		this.pedido = pedido;
	}
	
	@Column
	public BigDecimal getValUnitario() {
		return this.valUnitario;
	}
	public void setValUnitario(BigDecimal valor){
		this.valUnitario = valor;
	}
	

	@Column
	public String getNome() {
		return this.nome;
	}
	public void setNome(String nome){
		this.nome = nome;
	}
	
	public BigDecimal valTotal() {
		return this.getValUnitario().multiply(new BigDecimal(getQuantidade())).multiply(calculaTaxaDesconto(getValDesconto()));
	}

	@Column
	public BigDecimal getValDesconto() {
		return valDesconto;
	}
	public void setValDesconto(BigDecimal valDesconto) {
		this.valDesconto = valDesconto;
	}
	
	public BigDecimal calculaTaxaDesconto(BigDecimal taxa){
		if (taxa == null) {
			return new BigDecimal("1");
		}
		
		BigDecimal decimal = taxa.divide(new BigDecimal("100"));
		return new BigDecimal("1").subtract(decimal);
	}
}
