package com.fiap.model;

import javax.persistence.EntityManager;

import com.fiap.util.FabricaConexao;

public class PedidoDAO {

	public void salva(Pedido aInserir){
		EntityManager em = FabricaConexao.getEntityManager();
		em.getTransaction().begin();
		
		em.persist(aInserir);
		
		em.getTransaction().commit();
		em.close();
	}
	
//	public static void main(String[] args) {
//		Pedido p = new Pedido();
////		p.setData(new Date(2013, 1, 4));
//		p.setNome("Teste");
//		p.setPesfis(true);
////		p.setTotal(new BigDecimal("50"));
//		
//		EntityManager em = FabricaConexao.getEntityManager();
//		em.getTransaction().begin();
//
//		em.persist(p);
//		p.setNome("Gian");
//		em.persist(p);
//		
//		Item i = new Item();
//		i.setQuantidade(3);
//		
//		i.setPedido(p);
//		p.getItens().add(i);
//		
//		em.persist(i);
//		
//		
//		em.getTransaction().commit();
//		em.close();
//	}
}
