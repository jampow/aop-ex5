package com.fiap.model;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class ItemDataModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;

	private List<Item> linhas;

	private String[] colunas = new String[] { "Desc Produto", "Valor Unit.", "Quantidade", "Desconto", "Total" };

	public ItemDataModel() {
		linhas = new ArrayList<Item>();
	}

	public ItemDataModel(List<Item> itens) {
		linhas = new ArrayList<Item>(itens);
	}

	@Override
	public int getColumnCount() {
		return colunas.length;
	}

	@Override
	public int getRowCount() {
		return linhas.size();
	}

	@Override
	public String getColumnName(int columnIndex) {
		return colunas[columnIndex];
	}

	;

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return String.class;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Item itemSelecionado = linhas.get(rowIndex);

		switch (columnIndex) {

		// Seguindo o exemplo: "Desc Produto", "Valor Unit.", "Quantidade", "Desconto", "Total";
		case 0:
			return itemSelecionado.getNome();
		case 1:
			return itemSelecionado.getValUnitario();
		case 2:
			return itemSelecionado.getQuantidade();
		case 3:
			return itemSelecionado.getValDesconto();
		case 4:
			return itemSelecionado.valTotal();
		default:
			// Isto n�o deveria acontecer...
			throw new IndexOutOfBoundsException("columnIndex out of bounds");
		}
	}

	@Override
	// modifica na linha e coluna especificada
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		throw new UnsupportedOperationException("Tabela n�o edit�vel");
	}

	// modifica na linha especificada
	public void setValueAt(Item aValue, int rowIndex) {
		throw new UnsupportedOperationException("Tabela n�o edit�vel");
	};

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	public Item getItem(int indiceLinha) {
		return linhas.get(indiceLinha);
	}

	public void addItem(Item m) {
		linhas.add(m);

		int ultimoIndice = getRowCount() - 1;

		fireTableRowsInserted(ultimoIndice, ultimoIndice);
	}

	public void removeCliente(int indiceLinha) {
		linhas.remove(indiceLinha);

		fireTableRowsDeleted(indiceLinha, indiceLinha);
	}

	public void addListaDeCliente(List<Item> item) {
		int tamanhoAntigo = getRowCount();

		// Adiciona os registros.
		linhas.addAll(item);

		fireTableRowsInserted(tamanhoAntigo, getRowCount() - 1);
	}

	public void limpar() {
		linhas.clear();

		fireTableDataChanged();
	}

	public boolean isEmpty() {
		return linhas.isEmpty();
	}

}
