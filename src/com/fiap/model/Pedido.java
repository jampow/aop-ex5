package com.fiap.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

@Entity
@Table(name="pedido")
public class Pedido {
	private Long pedido_id;
	private Date data;
	private boolean pesfis;
	private String nome;
	private BigDecimal total;
	private BigDecimal desconto;
	private BigDecimal totalDesconto;
	private Set<Item> itens = new HashSet<Item>();
	
	public Pedido(){}
		
	@Id
	@GeneratedValue
	@Column
	public Long getPedidoId() {
		return pedido_id;
	}
	public void setPedidoId(Long id) {
		this.pedido_id = id;
	}

	@Column
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}

	@Column
	public boolean isPesfis() {
		return pesfis;
	}
	public void setPesfis(boolean pesfis) {
		this.pesfis = pesfis;
	}

	@Column
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Column
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	@Column
	public BigDecimal getTotalDesconto() {
		return totalDesconto;
	}
	public void setTotalDesconto(BigDecimal totalDesconto) {
		this.totalDesconto = totalDesconto;
	}

	@Column
	public BigDecimal getDesconto() {
		return desconto;
	}
	public void setDesconto(BigDecimal desconto) {
		this.desconto = desconto;
	}

	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
	@JoinColumn(name="pedidoId", insertable=true, updatable=true)
	public Set<Item> getItens() {
		return itens;
	}
	public void setItens(Set<Item> itens) {
		this.itens = itens;
	}
}
