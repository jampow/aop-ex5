package com.fiap.model;

import java.math.BigDecimal;

import javax.persistence.EntityManager;

import com.fiap.util.FabricaConexao;


public class ItemDAO {
	
	
	public static void main(String[] args) {
		Produto p = new Produto();
		p.setNome("Livro");
		p.setValor(new BigDecimal("100.00"));
		
		EntityManager em = FabricaConexao.getEntityManager();
		em.getTransaction().begin();

		em.persist(p);
		
		
		
		Item i = new Item();
		i.setQuantidade(new Long("2"));
		i.setValDesconto(new BigDecimal("10"));
		System.out.println(i.valTotal());
		
		em.persist(i);
		i.setValDesconto(new BigDecimal("20"));
		em.persist(i);
		
		em.getTransaction().commit();
		
		System.out.println(i.valTotal());
	}
	


}
