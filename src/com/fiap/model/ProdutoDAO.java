package com.fiap.model;

import java.util.List;

import javax.persistence.EntityManager;

import com.fiap.util.FabricaConexao;

public class ProdutoDAO {

	public static List<Produto> listar(){
		EntityManager em = FabricaConexao.getEntityManager();
		return em.createQuery("from Produto").getResultList();
	}

}
