package com.fiap.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="erro")
public class Erro {
	private Long erro_id;
	private Date data;
	private String mensagem;
	
	public Erro(){}

	@Id
	@GeneratedValue
	@Column
	public Long getErro_id() {
		return erro_id;
	}

	public void setErro_id(Long erro_id) {
		this.erro_id = erro_id;
	}

	@Column(name="data_erro")
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	@Column
	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	

}
