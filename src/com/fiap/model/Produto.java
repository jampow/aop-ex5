package com.fiap.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="produto")
public class Produto {
	private Long produto_id;
	private String nome;
	private BigDecimal valor;
	
	public Produto(){}
	
	@Id
	@GeneratedValue
	@Column
	public Long getProdutoId() {
		return produto_id;
	}
	public void setProdutoId(Long id) {
		this.produto_id = id;
	}

	@Column
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Column
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
	
	public String toString(){
		return getNome();
	}
}
