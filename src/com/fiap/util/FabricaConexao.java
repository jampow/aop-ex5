package com.fiap.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class FabricaConexao {
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("conexao");

	private FabricaConexao() {}
	
	public static EntityManager getEntityManager(){
		return emf.createEntityManager();
	}
}
