package com.fiap.util;

import java.awt.EventQueue;
import java.math.BigDecimal;

import javax.persistence.EntityManager;

import com.fiap.model.Produto;
import com.fiap.view.PedidoVenda;

public class Start {

	public static void main(String[] args) {
		EntityManager em = FabricaConexao.getEntityManager();
		em.getTransaction().begin();
		
		Produto pro = new Produto();
		pro.setNome("AspectJ � a arte de Pensar Diferente");
		pro.setValor(new BigDecimal("208.07"));

		em.persist(pro);

		Produto pro1 = new Produto();
		pro1.setNome("AspectJ - Programa��o orientada a aspectos com Java");
		pro1.setValor(new BigDecimal("305.80"));

		em.persist(pro1);

		Produto pro2 = new Produto();
		pro2.setNome("Vire o jogo com Spring Framework");
		pro2.setValor(new BigDecimal("467.98"));

		em.persist(pro2);
		
		Produto pro3 = new Produto();
		pro3.setNome("Spring em a��o");
		pro3.setValor(new BigDecimal("156.75"));

		em.persist(pro3);
		
		Produto pro4 = new Produto();
		pro4.setNome("Programa��o Java para a Web");
		pro4.setValor(new BigDecimal("159.64"));
	
		em.persist(pro4);
		
		Produto pro5 = new Produto();
		pro5.setNome("Livro teste seu c�digo");
		pro5.setValor(new BigDecimal("1"));
	
		em.persist(pro5);
		
		em.getTransaction().commit();
		em.close();


		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PedidoVenda window = new PedidoVenda();
					window.getFrmRegistroDePedido().setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
