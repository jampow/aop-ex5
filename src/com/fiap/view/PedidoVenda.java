package com.fiap.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import com.fiap.model.Item;
import com.fiap.model.ItemDataModel;
import com.fiap.model.Pedido;
import com.fiap.model.PedidoDAO;
import com.fiap.model.Produto;
import com.fiap.model.ProdutoDAO;

public class PedidoVenda {

	private JFrame frmRegistroDePedido;
	private JTextField txtNroPedido;
	private JLabel lblData;
	private JTextField txtData;
	private JComboBox cmbTipo;
	private JLabel lblNome;
	private JTextField txtNome;
	private JLabel lblProduto;
	private JLabel lblQuantidade;
	private JTextField txtQuantidade;
	private JButton btnAdiciona;
	private JTable tblItens;
	private JPanel pnlEspacamento;
	private JPanel pnlEspacamento2;
	private JLabel lblTotalGeral;
	private JLabel lblResultadoTotal;
	private JButton btnFinalizar;
	private JComboBox cmbProduto;
	
	private Pedido pedido;
	
	private ItemDataModel itemDataModel = new ItemDataModel();

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					PedidoVenda window = new PedidoVenda();
//					window.getFrmRegistroDePedido().setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the application.
	 */
	public PedidoVenda() {
		pedido = new Pedido();
		initialize();
	}
	
	private void adicionaListeners(){
		txtData.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				SimpleDateFormat sdf = new SimpleDateFormat("d/M/y");
				
				try {
					pedido.setData(sdf.parse(txtData.getText()));
				} catch (ParseException e) {
					throw new RuntimeException("Data inv�lida");
				}
			}
		});
		
		cmbTipo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				pedido.setPesfis(cmbTipo.getSelectedItem().toString() == "F\u00EDsico") ;			
			}
		});
		
		txtNome.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				pedido.setNome(txtNome.getText());
			}
		});
		
		btnAdiciona.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Produto produtoSelecionado = (Produto)cmbProduto.getSelectedItem();
				
				Item novoItem = new Item();
				novoItem.setQuantidade(Long.parseLong(txtQuantidade.getText()));
				novoItem.setNome(produtoSelecionado.getNome());
				novoItem.setValUnitario(produtoSelecionado.getValor());
				
				pedido.getItens().add(novoItem);
				
				itemDataModel.addItem(novoItem);
				
				BigDecimal total = new BigDecimal("0");
				for (Item item : pedido.getItens()) {
					total = total.add(item.valTotal());
				}
				
				lblResultadoTotal.setText(total.toPlainString());
			}
		});
		
		btnFinalizar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				pedido.setNome(txtNome.getText());
				pedido.setPesfis("f�sica".equals(cmbTipo.getSelectedItem()));
				pedido.setTotal(new BigDecimal(lblResultadoTotal.getText()));
				
				new PedidoDAO().salva(pedido);
			}
		});
	}
	
	private Produto[] carregaComboProduto(){
		List<Produto> produtosDB = new ProdutoDAO().listar();
		Produto[] produtos = new Produto[produtosDB.size()];
		
		for (int i = 0; i < produtosDB.size(); i++) {
			produtos[i] = produtosDB.get(i);
		}
		
		return produtos;
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setFrmRegistroDePedido(new JFrame());
		getFrmRegistroDePedido().getContentPane().setFont(new Font("SansSerif", Font.BOLD, 12));
		getFrmRegistroDePedido().setTitle("Registro de pedido");
		getFrmRegistroDePedido().setBounds(100, 100, 501, 301);
		getFrmRegistroDePedido().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 66, 0, 67, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		getFrmRegistroDePedido().getContentPane().setLayout(gridBagLayout);
		
		pnlEspacamento = new JPanel();
		GridBagConstraints gbc_pnlEspacamento = new GridBagConstraints();
		gbc_pnlEspacamento.insets = new Insets(0, 0, 5, 5);
		gbc_pnlEspacamento.gridx = 0;
		gbc_pnlEspacamento.gridy = 0;
		getFrmRegistroDePedido().getContentPane().add(pnlEspacamento, gbc_pnlEspacamento);
		
		JLabel lblNroPed = new JLabel("Nro. Pedido");
		lblNroPed.setFont(new Font("SansSerif", Font.BOLD, 12));
		GridBagConstraints gbc_lblNroPed = new GridBagConstraints();
		gbc_lblNroPed.anchor = GridBagConstraints.WEST;
		gbc_lblNroPed.insets = new Insets(0, 0, 5, 5);
		gbc_lblNroPed.gridx = 1;
		gbc_lblNroPed.gridy = 1;
		getFrmRegistroDePedido().getContentPane().add(lblNroPed, gbc_lblNroPed);
		
		txtNroPedido = new JTextField();
		GridBagConstraints gbc_txtNroPedido = new GridBagConstraints();
		gbc_txtNroPedido.gridwidth = 2;
		gbc_txtNroPedido.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtNroPedido.insets = new Insets(0, 0, 5, 5);
		gbc_txtNroPedido.gridx = 2;
		gbc_txtNroPedido.gridy = 1;
		getFrmRegistroDePedido().getContentPane().add(txtNroPedido, gbc_txtNroPedido);
		txtNroPedido.setColumns(10);
		
		lblData = new JLabel("Data");
		lblData.setFont(new Font("SansSerif", Font.BOLD, 12));
		GridBagConstraints gbc_lblData = new GridBagConstraints();
		gbc_lblData.anchor = GridBagConstraints.WEST;
		gbc_lblData.insets = new Insets(0, 0, 5, 5);
		gbc_lblData.gridx = 4;
		gbc_lblData.gridy = 1;
		getFrmRegistroDePedido().getContentPane().add(lblData, gbc_lblData);
		
		txtData = new JTextField();
		GridBagConstraints gbc_txtData = new GridBagConstraints();
		gbc_txtData.gridwidth = 2;
		gbc_txtData.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtData.insets = new Insets(0, 0, 5, 5);
		gbc_txtData.gridx = 5;
		gbc_txtData.gridy = 1;
		getFrmRegistroDePedido().getContentPane().add(txtData, gbc_txtData);
		txtData.setColumns(10);
		
		JLabel lblTipoCliente = new JLabel("Tipo Cliente");
		lblTipoCliente.setFont(new Font("SansSerif", Font.BOLD, 12));
		GridBagConstraints gbc_lblTipoCliente = new GridBagConstraints();
		gbc_lblTipoCliente.anchor = GridBagConstraints.WEST;
		gbc_lblTipoCliente.insets = new Insets(0, 0, 5, 5);
		gbc_lblTipoCliente.gridx = 7;
		gbc_lblTipoCliente.gridy = 1;
		getFrmRegistroDePedido().getContentPane().add(lblTipoCliente, gbc_lblTipoCliente);
		
		cmbTipo = new JComboBox();
		cmbTipo.setModel(new DefaultComboBoxModel(new String[] {"F\u00EDsico", "Jur\u00EDcido"}));
		GridBagConstraints gbc_cmbTipo = new GridBagConstraints();
		gbc_cmbTipo.fill = GridBagConstraints.HORIZONTAL;
		gbc_cmbTipo.insets = new Insets(0, 0, 5, 5);
		gbc_cmbTipo.gridx = 8;
		gbc_cmbTipo.gridy = 1;
		getFrmRegistroDePedido().getContentPane().add(cmbTipo, gbc_cmbTipo);
		
		lblNome = new JLabel("Nome");
		lblNome.setFont(new Font("SansSerif", Font.BOLD, 12));
		GridBagConstraints gbc_lblNome = new GridBagConstraints();
		gbc_lblNome.anchor = GridBagConstraints.WEST;
		gbc_lblNome.insets = new Insets(0, 0, 5, 5);
		gbc_lblNome.gridx = 1;
		gbc_lblNome.gridy = 2;
		getFrmRegistroDePedido().getContentPane().add(lblNome, gbc_lblNome);
		
		txtNome = new JTextField();
		GridBagConstraints gbc_txtNome = new GridBagConstraints();
		gbc_txtNome.insets = new Insets(0, 0, 5, 5);
		gbc_txtNome.gridwidth = 7;
		gbc_txtNome.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtNome.gridx = 2;
		gbc_txtNome.gridy = 2;
		getFrmRegistroDePedido().getContentPane().add(txtNome, gbc_txtNome);
		txtNome.setColumns(10);
		
		lblProduto = new JLabel("Produto");
		lblProduto.setFont(new Font("SansSerif", Font.BOLD, 12));
		GridBagConstraints gbc_lblProduto = new GridBagConstraints();
		gbc_lblProduto.anchor = GridBagConstraints.WEST;
		gbc_lblProduto.insets = new Insets(0, 0, 5, 5);
		gbc_lblProduto.gridx = 1;
		gbc_lblProduto.gridy = 3;
		getFrmRegistroDePedido().getContentPane().add(lblProduto, gbc_lblProduto);
		
		cmbProduto = new JComboBox();
		cmbProduto.setModel(new DefaultComboBoxModel(carregaComboProduto()));
		cmbProduto.setSelectedIndex(0);
		cmbProduto.setEditable(true);
		GridBagConstraints gbc_cmbProduto = new GridBagConstraints();
		gbc_cmbProduto.gridwidth = 5;
		gbc_cmbProduto.insets = new Insets(0, 0, 5, 5);
		gbc_cmbProduto.fill = GridBagConstraints.HORIZONTAL;
		gbc_cmbProduto.gridx = 2;
		gbc_cmbProduto.gridy = 3;
		getFrmRegistroDePedido().getContentPane().add(cmbProduto, gbc_cmbProduto);
		
		lblQuantidade = new JLabel("Quantidade");
		lblQuantidade.setFont(new Font("SansSerif", Font.BOLD, 12));
		GridBagConstraints gbc_lblQuantidade = new GridBagConstraints();
		gbc_lblQuantidade.insets = new Insets(0, 0, 5, 5);
		gbc_lblQuantidade.anchor = GridBagConstraints.WEST;
		gbc_lblQuantidade.gridx = 7;
		gbc_lblQuantidade.gridy = 3;
		getFrmRegistroDePedido().getContentPane().add(lblQuantidade, gbc_lblQuantidade);
		
		txtQuantidade = new JTextField();
		GridBagConstraints gbc_txtQuantidade = new GridBagConstraints();
		gbc_txtQuantidade.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtQuantidade.insets = new Insets(0, 0, 5, 5);
		gbc_txtQuantidade.gridx = 8;
		gbc_txtQuantidade.gridy = 3;
		getFrmRegistroDePedido().getContentPane().add(txtQuantidade, gbc_txtQuantidade);
		txtQuantidade.setColumns(10);
		
		tblItens = new JTable();
		tblItens.setBorder(new LineBorder(new Color(0, 0, 0)));
		tblItens.setSurrendersFocusOnKeystroke(true);
		tblItens.setFillsViewportHeight(true);
		tblItens.setColumnSelectionAllowed(true);
		tblItens.setCellSelectionEnabled(true);
		tblItens.setShowVerticalLines(true);
		tblItens.setShowHorizontalLines(true);
		tblItens.setModel(itemDataModel);
		tblItens.getColumnModel().getColumn(0).setPreferredWidth(54);
		tblItens.getColumnModel().getColumn(1).setPreferredWidth(159);
		tblItens.getColumnModel().getColumn(2).setPreferredWidth(82);
		tblItens.getColumnModel().getColumn(4).setPreferredWidth(77);
		GridBagConstraints gbc_tblItens = new GridBagConstraints();
		gbc_tblItens.gridwidth = 8;
		gbc_tblItens.insets = new Insets(0, 0, 5, 5);
		gbc_tblItens.fill = GridBagConstraints.BOTH;
		gbc_tblItens.gridx = 1;
		gbc_tblItens.gridy = 4;
		getFrmRegistroDePedido().getContentPane().add(tblItens, gbc_tblItens);
		
		btnAdiciona = new JButton("Adicionar Produto");
		GridBagConstraints gbc_btnAdiciona = new GridBagConstraints();
		gbc_btnAdiciona.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnAdiciona.gridwidth = 2;
		gbc_btnAdiciona.anchor = GridBagConstraints.SOUTH;
		gbc_btnAdiciona.insets = new Insets(0, 0, 5, 5);
		gbc_btnAdiciona.gridx = 1;
		gbc_btnAdiciona.gridy = 5;
		getFrmRegistroDePedido().getContentPane().add(btnAdiciona, gbc_btnAdiciona);
		
		btnFinalizar = new JButton("Finalizar");
		GridBagConstraints gbc_btnFinalizar = new GridBagConstraints();
		gbc_btnFinalizar.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnFinalizar.gridwidth = 3;
		gbc_btnFinalizar.insets = new Insets(0, 0, 5, 5);
		gbc_btnFinalizar.gridx = 3;
		gbc_btnFinalizar.gridy = 5;
		getFrmRegistroDePedido().getContentPane().add(btnFinalizar, gbc_btnFinalizar);
		
		lblTotalGeral = new JLabel("Total Geral");
		lblTotalGeral.setFont(new Font("SansSerif", Font.BOLD, 12));
		GridBagConstraints gbc_lblTotalGeral = new GridBagConstraints();
		gbc_lblTotalGeral.anchor = GridBagConstraints.EAST;
		gbc_lblTotalGeral.insets = new Insets(0, 0, 5, 5);
		gbc_lblTotalGeral.gridx = 6;
		gbc_lblTotalGeral.gridy = 5;
		getFrmRegistroDePedido().getContentPane().add(lblTotalGeral, gbc_lblTotalGeral);
		
		lblResultadoTotal = new JLabel("R$ 0,00");
		lblResultadoTotal.setFont(new Font("SansSerif", Font.PLAIN, 12));
		GridBagConstraints gbc_lblResultadoTotal = new GridBagConstraints();
		gbc_lblResultadoTotal.gridwidth = 2;
		gbc_lblResultadoTotal.anchor = GridBagConstraints.EAST;
		gbc_lblResultadoTotal.insets = new Insets(0, 0, 5, 5);
		gbc_lblResultadoTotal.gridx = 7;
		gbc_lblResultadoTotal.gridy = 5;
		getFrmRegistroDePedido().getContentPane().add(lblResultadoTotal, gbc_lblResultadoTotal);
		
		pnlEspacamento2 = new JPanel();
		GridBagConstraints gbc_pnlEspacamento2 = new GridBagConstraints();
		gbc_pnlEspacamento2.fill = GridBagConstraints.BOTH;
		gbc_pnlEspacamento2.gridx = 9;
		gbc_pnlEspacamento2.gridy = 6;
		getFrmRegistroDePedido().getContentPane().add(pnlEspacamento2, gbc_pnlEspacamento2);
		
		adicionaListeners();		
	}

	public JFrame getFrmRegistroDePedido() {
		return frmRegistroDePedido;
	}

	public void setFrmRegistroDePedido(JFrame frmRegistroDePedido) {
		this.frmRegistroDePedido = frmRegistroDePedido;
	}

}
